# The following args have been provided default values but can be overriden
# from the CLI by specifying those variables with the override values.

# Prefix for Docker images
PROJECT ?= deno

# Version to use for tagging images.
VERSION ?= 0.30.1

DOCKER_CONTAINER_HOME_PATH = /home/appuser

DOCKER_HOST_PROJECT_PATH = ${PWD}

DOCKER_IMAGE_NAME = ${PROJECT}
DOCKER_IMAGE_TAG = ${VERSION}

DOCKER_ARGS = \
	-e "PROJECT=${PROJECT}" \
	-e "HOME=${DOCKER_CONTAINER_HOME_PATH}" \
	-u `id -u`:`id -g` \
	-v ${DOCKER_HOST_PROJECT_PATH}/.homedir:${DOCKER_CONTAINER_HOME_PATH} \
	-v ${DOCKER_HOST_PROJECT_PATH}/examples:/opt/${PROJECT}-examples \
	-w /opt/${PROJECT}-examples

# Cleans up the temporary files
clean:
	rm -rf .local .cache

bash:
	docker run \
		${DOCKER_ARGS} \
		--rm -it \
		${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} bash

image:
	docker build \
		--build-arg="VERSION=${VERSION}" \
		--rm \
		--tag=${DOCKER_IMAGE_NAME} \
		--tag=${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} \
		.

.PHONY: image bash clean run
# vi: set nowrap:
